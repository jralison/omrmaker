<?php

/**
 * Created by PhpStorm.
 * User: Jonathan Souza
 * Date: 27/12/2016
 * Time: 16:40
 */
class OmrSheetTest extends \PHPUnit_Framework_TestCase {

    private $page_width;
    private $page_height;
    private $margin_left;
    private $margin_right;
    private $margin_top;
    private $margin_bottom;

    public function setUp() {
        $this->page_width = 210;
        $this->page_height = 297;

        $this->margin_left = 15;
        $this->margin_right = 25;
        $this->margin_top = 15;
        $this->margin_bottom = 25;
    }

    public function testIfLayoutManagerIsProperlySetOnInitialization() {
        $lm = new \App\Layouts\AbsoluteLayout();
        $sheet = new \App\OmrSheet($this->page_width, $this->page_height, $lm);
        self::assertSame($lm, $sheet->getLayoutManager());
    }

    public function testIfPageDimensionsAreProperlySetAndReturned() {
        // On initialization
        $sheet = new \App\OmrSheet($this->page_width, $this->page_height);
        self::assertEquals($this->page_width, $sheet->getPageWidth());
        self::assertEquals($this->page_height, $sheet->getPageHeight());

        // Change to Letter
        $sheet->setPageWidth(216)->setPageHeight(279);
        self::assertEquals(216, $sheet->getPageWidth());
        self::assertEquals(279, $sheet->getPageHeight());
    }

    public function testIfMarginsAreProperlySetAndReturned() {
        $sheet = new \App\OmrSheet($this->page_width, $this->page_height);

        $sheet->setMarginTop($this->margin_top);
        self::assertEquals($this->margin_top, $sheet->getMarginTop());

        $sheet->setMarginLeft($this->margin_left);
        self::assertEquals($this->margin_left, $sheet->getMarginLeft());

        $sheet->setMarginRight($this->margin_right);
        self::assertEquals($this->margin_right, $sheet->getMarginRight());

        $sheet->setMarginBottom($this->margin_bottom);
        self::assertEquals($this->margin_bottom, $sheet->getMarginBottom());
    }

    public function testIfSafeDimensionsAreProperlyCalculated() {
        $sheet = new \App\OmrSheet($this->page_width, $this->page_height);
        $sheet->setMarginTop($this->margin_top);
        $sheet->setMarginLeft($this->margin_left);
        $sheet->setMarginRight($this->margin_right);
        $sheet->setMarginBottom($this->margin_bottom);

        $safe_width_expected = ($this->page_width - ($this->margin_left + $this->margin_right));
        self::assertEquals($safe_width_expected, $sheet->getSafePageWidth());

        $safe_height_expected = ($this->page_height - ($this->margin_top + $this->margin_bottom));
        self::assertEquals($safe_height_expected, $sheet->getSafePageHeight());
    }

}
