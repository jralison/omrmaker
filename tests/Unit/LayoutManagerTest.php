<?php
/**
 * Created by PhpStorm.
 * User: Jonathan Souza
 * Date: 28/12/2016
 * Time: 13:21
 */

use App\Elements\Box;
use App\OmrSheet;


class LayoutManagerTest extends \PHPUnit_Framework_TestCase {

    public function testIfAddedBoxesAreProperlyCounted() {
        $sheet = new OmrSheet(210, 297);
        $lm = $sheet->getLayoutManager();

        // Garante que o layout está vazio
        self::assertEquals(0, $lm->countBoxes());

        // Cria as boxes
        $box1 = new Box(60, 60);
        $box2 = new Box(100, 100);
        $box3 = new Box(150, 200);
        $box4 = new Box(52, 17);

        // Adiciona as boxes criadas e conta
        $lm->add($box1, 0, 0)
            ->add($box2, 0, 60)
            ->add($box3, 0, 160)
            ->add($box4, 0, 360);
        self::assertEquals(4, $sheet->getLayoutManager()->countBoxes());

        // Remove dois boxes e conta
        $lm->remove($box3)
            ->remove($box1);
        self::assertEquals(2, $lm->countBoxes());
    }

    public function testIfBoxesAreProperlyRemovedFromLayout() {
        $sheet = new OmrSheet(210, 297);
        $lm = $sheet->getLayoutManager();

        // Garante que o layout está vazio
        self::assertEmpty($lm->getArrayOfBoxes());

        // Cria as boxes
        $box1 = new Box(60, 60);
        $box2 = new Box(100, 100);
        $box3 = new Box(150, 200);
        $box4 = new Box(52, 17);

        // Adiciona as boxes criadas
        $lm->add($box1, 0, 0)
            ->add($box2, 0, 60)
            ->add($box3, 0, 160)
            ->add($box4, 0, 360);

        // Remove dois boxes e conta
        $lm->remove($box3)
            ->remove($box1);

        // Checa se as boxes removidas não estão presentes e se as demais permanecem no layout
        $added_boxes = $lm->getArrayOfBoxes();
        self::assertNotContains($box1, $added_boxes);
        self::assertNotContains($box3, $added_boxes);
        self::assertContains($box2, $added_boxes);
        self::assertContains($box4, $added_boxes);
    }

    public function testIfAllAddedBoxesAreReturnedInArray() {
        $sheet = new OmrSheet(210, 297);
        $lm = $sheet->getLayoutManager();

        // Garante que não há box no layout
        self::assertEmpty($lm->getArrayOfBoxes());

        // Cria as boxes
        $box1 = new Box(60, 60);
        $box2 = new Box(100, 100);
        $box3 = new Box(150, 200);
        $box4 = new Box(52, 17);

        // Adiciona as boxes criadas
        $lm->add($box1, 0, 0)
            ->add($box2, 0, 60)
            ->add($box3, 0, 160)
            ->add($box4, 0, 360);

        // Checa se cada box está contida no array retornado
        $added_boxes = $lm->getArrayOfBoxes();
        self::assertContains($box1, $added_boxes);
        self::assertContains($box2, $added_boxes);
        self::assertContains($box3, $added_boxes);
        self::assertContains($box4, $added_boxes);
    }

}
