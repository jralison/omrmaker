<?php
/**
 *
 * @author jonat
 */

namespace unit;

use App\Elements\Box;
use App\OmrSheet;

class AbsoluteLayoutTest extends \PHPUnit_Framework_TestCase {

    public function testIfNegativeHorizontalPositioningIsRefusedWithException() {
        $sheet = new OmrSheet(210, 297);
        try {
            $sheet->getLayoutManager()->add(new Box(10, 10), -10, 100);
        } catch (\InvalidArgumentException $ex) {
            self::assertInstanceOf(\InvalidArgumentException::class, $ex);
        }
    }

    public function testIfNegativeVerticalPositioningIsRefusedWithException() {
        $sheet = new OmrSheet(210, 297);
        try {
            $sheet->getLayoutManager()->add(new Box(10, 10), 0, -100);
        } catch (\InvalidArgumentException $ex) {
            self::assertInstanceOf(\InvalidArgumentException::class, $ex);
        }
    }

}
