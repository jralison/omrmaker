<?php
/**
 * Created by PhpStorm.
 * User: Jonathan Souza
 * Date: 03/01/2017
 * Time: 16:25
 */

namespace App\Exports;

use App\OmrSheet;

abstract class ExportDriver {

    /** @var OmrSheet */
    private $sheet;

    public function __construct(OmrSheet $sheet) {
        $this->sheet = $sheet;
    }

    /**
     * @return OmrSheet
     */
    public function getSheet(): OmrSheet {
        return $this->sheet;
    }

    /**
     * @param OmrSheet $sheet
     */
    public function setSheet(OmrSheet $sheet) {
        $this->sheet = $sheet;
    }

    public abstract function exportWireframe();

}
