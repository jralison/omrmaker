<?php
/**
 * Created by PhpStorm.
 * User: Jonathan Souza
 * Date: 03/01/2017
 * Time: 16:34
 */

namespace App\Exports;

use App\Converter\MmToPx;
use App\Elements\Box;
use Intervention\Image\AbstractShape;
use Intervention\Image\ImageManager;

class ImageExporter extends ExportDriver {

    public function exportWireframe() {
        $sheet = $this->getSheet();
        $unitConverter = new MmToPx();
        $imageManager = new ImageManager(['driver' => 'gd']);

        $canvas = $imageManager->canvas(
            $unitConverter->convert($sheet->getSafePageWidth()),
            $unitConverter->convert($sheet->getSafePageHeight()),
            '#ffffff'
        );

        foreach ($sheet->getLayoutManager()->getArrayOfBoxes(true) as $data) {
            /** @var Box $box */
            $box = $data['box'];
            $info = $data['info'];
            $canvas->rectangle(
                $unitConverter->convert($info['x']) + 1,
                $unitConverter->convert($info['y']) + 1,
                $unitConverter->convert($info['x'] + $box->getWidth()) - 1,
                $unitConverter->convert($info['y'] + $box->getHeight()) - 1,
                function ($draw) {
                    /** @var $draw AbstractShape */
                    $draw->border(1, '#f03e3e');
                    $draw->background('#e9ecef');
                }
            );
        }

        $canvas->resizeCanvas(
            $unitConverter->convert($sheet->getPageWidth()),
            $unitConverter->convert($sheet->getPageHeight()),
            'center',
            false,
            '#adb5bd'
        );
        $canvas->save('omr.png', 100);
        $canvas->destroy();
    }

}
