<?php
/**
 * Created by PhpStorm.
 * User: Jonathan Souza
 * Date: 26/12/2016
 * Time: 17:13
 */

namespace App;

use App\Layouts\AbsoluteLayout;
use App\Layouts\LayoutManager;

class OmrSheet {

    /** @var float page width */
    private $page_width;

    /** @var float page height */
    private $page_height;

    /** @var float left margin width */
    private $margin_left;

    /** @var float right margin width */
    private $margin_right;

    /** @var float top margin height */
    private $margin_top;

    /** @var float bottom margin height */
    private $margin_bottom;

    /** @var AbsoluteLayout the layout manager to manage elements positioning */
    private $layout_manager;

    /**
     * OmrSheet constructor.
     * @param float $width
     * @param float $height
     * @param LayoutManager $layout_manager Defaults to NULL, meaning to use a BasicLayoutManager
     */
    public function __construct(float $width, float $height, LayoutManager $layout_manager = null) {
        $this->page_width = $width;
        $this->page_height = $height;

        if (is_null($layout_manager))
            $this->layout_manager = new AbsoluteLayout();
        else
            $this->layout_manager = $layout_manager;

        return $this;
    }

    /**
     * Returns the page width minus the sum of left and right margins.
     * @return float
     */
    public function getSafePageWidth(): float {
        return ($this->page_width - ($this->margin_left + $this->margin_right));
    }

    /**
     * Returns the page height minus the sum of top and bottom margins.
     * @return float
     */
    public function getSafePageHeight(): float {
        return ($this->page_height - ($this->margin_top + $this->margin_bottom));
    }

    /**
     * @return float
     */
    public function getPageWidth(): float {
        return $this->page_width;
    }

    /**
     * Sets a new width for the page and therefore updates the page width on the layout manager.
     *
     * @param float $page_width
     * @return OmrSheet
     */
    public function setPageWidth(float $page_width): OmrSheet {
        $this->page_width = $page_width;
        return $this;
    }

    /**
     * @return float
     */
    public function getPageHeight(): float {
        return $this->page_height;
    }

    /**
     * Sets a new height for the page and therefore updates the page height on the layout manager.
     *
     * @param float $page_height
     * @return OmrSheet
     */
    public function setPageHeight(float $page_height): OmrSheet {
        $this->page_height = $page_height;
        return $this;
    }

    /**
     * @return float
     */
    public function getMarginLeft(): float {
        return $this->margin_left;
    }

    /**
     * @param float $margin_left
     * @return OmrSheet
     */
    public function setMarginLeft(float $margin_left): OmrSheet {
        $this->margin_left = $margin_left;
        return $this;
    }

    /**
     * @return float
     */
    public function getMarginRight(): float {
        return $this->margin_right;
    }

    /**
     * @param float $margin_right
     * @return OmrSheet
     */
    public function setMarginRight(float $margin_right): OmrSheet {
        $this->margin_right = $margin_right;
        return $this;
    }

    /**
     * @return float
     */
    public function getMarginTop(): float {
        return $this->margin_top;
    }

    /**
     * @param float $margin_top
     * @return OmrSheet
     */
    public function setMarginTop(float $margin_top): OmrSheet {
        $this->margin_top = $margin_top;
        return $this;
    }

    /**
     * @return float
     */
    public function getMarginBottom(): float {
        return $this->margin_bottom;
    }

    /**
     * @param float $margin_bottom
     * @return OmrSheet
     */
    public function setMarginBottom(float $margin_bottom): OmrSheet {
        $this->margin_bottom = $margin_bottom;
        return $this;
    }

    /**
     * @return AbsoluteLayout
     */
    public function getLayoutManager(): AbsoluteLayout {
        return $this->layout_manager;
    }

    /**
     * Sets all four margins at once. If bottom and right margins are not supplied, they assume the top and left margins
     * respectively.
     *
     * @param float $top
     * @param float $left
     * @param float $bottom the bottom margin or NULL to keep the same as the top margin
     * @param float $right the right margin or NULL to keep the same as the left margin
     * @return $this
     */
    public function setMargins(float $top, float $left, float $bottom = null, float $right = null) {
        $this->setMarginTop($top);
        $this->setMarginLeft($left);
        $this->setMarginRight(is_null($right) ? $left : $right);
        $this->setMarginBottom(is_null($bottom) ? $top : $bottom);
        return $this;
    }

}
