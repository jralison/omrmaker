<?php
/**
 * Created by PhpStorm.
 * User: Jonathan Souza
 * Date: 26/12/2016
 * Time: 17:18
 */

namespace App\Layouts;

use App\Elements\Box;

class AbsoluteLayout extends LayoutManager {

    /**
     * @param Box $box
     * @param float $x
     * @param float $y
     * @return AbsoluteLayout
     */
    public function add(Box $box, float $x, float $y): AbsoluteLayout {
        if ($x < 0 || $y < 0)
            throw new \InvalidArgumentException('Both x and y positions must be greater than or equal zero');
        $this->attachBox($box, compact('x', 'y'));
        return $this;
    }

    /**
     * @param Box $box
     * @return AbsoluteLayout
     */
    public function remove(Box $box): AbsoluteLayout {
        $this->detachBox($box);
        return $this;
    }

}
