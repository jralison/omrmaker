<?php
/**
 * Created by PhpStorm.
 * User: Jonathan Souza
 * Date: 28/12/2016
 * Time: 16:48
 */

namespace App\Layouts;

use App\Elements\Box;

abstract class LayoutManager {

    /** @var \SplObjectStorage the repository with added boxes to the sheet */
    private $_boxes;

    /**
     * LayoutManager constructor.
     */
    public function __construct() {
        $this->_boxes = new \SplObjectStorage();
    }

    /**
     * @return int
     */
    public function countBoxes(): int {
        return $this->_boxes->count();
    }

    /**
     * @param bool $with_info
     * @return Box[]|array
     */
    public function getArrayOfBoxes($with_info = false): array {
        $boxes = [];
        $this->_boxes->rewind();
        while ($this->_boxes->valid()) {
            if ($with_info)
                $boxes[] = ['box' => $this->_boxes->current(), 'info' => $this->_boxes->getInfo()];
            else
                $boxes[] = $this->_boxes->current();
            $this->_boxes->next();
        }
        return $boxes;
    }

    /**
     * Internally used to attach a Box to the layout. It exists so it does not restrict the implementation of the add()
     * method, allowing each LayoutManager to manage theirs layout_params on their own.
     *
     * @param Box $box
     * @param mixed $layout_params
     */
    protected function attachBox(Box $box, $layout_params = null) {
        $this->_boxes->attach($box, $layout_params);
    }

    /**
     * Internally used to remove a Box from the layout.
     * @param Box $box
     */
    protected function detachBox(Box $box) {
        $this->_boxes->detach($box);
    }

}
