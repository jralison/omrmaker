<?php

namespace App\Elements;

/**
 * Represents any box that can be added to an OMR Sheet. A box is basically a Rectangle except that it can contains a
 * certain kind of elements in it. For example, a TextBox contains a text and a BubbleBox contains a set of bubbles to
 * be filled.
 *
 * @package App\Elements
 */
class Box {

    /** @var float */
    private $height;

    /** @var float */
    private $width;

    /**
     * Box constructor.
     * @param float $width
     * @param float $height
     */
    public function __construct(float $width, float $height) {
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * @return float
     */
    public function getHeight(): float {
        return $this->height;
    }

    /**
     * @param float $height
     */
    public function setHeight(float $height) {
        $this->height = $height;
    }

    /**
     * @return float
     */
    public function getWidth(): float {
        return $this->width;
    }

    /**
     * @param float $width
     */
    public function setWidth(float $width) {
        $this->width = $width;
    }

}
