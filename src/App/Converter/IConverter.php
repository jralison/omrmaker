<?php
/**
 * Created by PhpStorm.
 * User: Jonathan Souza
 * Date: 02/01/2017
 * Time: 17:41
 */

namespace App\Converter;

interface IConverter {

    public function convert($mm);

}
