<?php
/**
 * Created by PhpStorm.
 * User: Jonathan Souza
 * Date: 02/01/2017
 * Time: 17:43
 */

namespace App\Converter;

class MmToPx implements IConverter {

    // 1mm = 0.0393700787in
    const RATIO = 0.0393700787;

    /** @var int Pixels Per Inch */
    private $ppi = 600;

    public function getPpi(): int {
        return $this->ppi;
    }

    public function setPpi(int $ppi) {
        $this->ppi = $ppi;
    }

    /**
     * @param $mm
     * @return int
     */
    public function convert($mm) {
        // mm to inches
        $inches = ($mm * self::RATIO);

        // inches to pixels
        $pixels = ($inches * $this->ppi);

        return intval(round($pixels));
    }
}
